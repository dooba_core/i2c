/* Dooba SDK
 * I2C Communication
 */

// External Includes
#include <util/delay.h>

// Internal Includes
#include "i2c.h"

// Initialize I2C Interface
void i2c_init()
{
	// Set Prescaler
	TWSR = 0x00;
	TWBR = I2C_PRESCALER;

	// Enable I2C
	TWCR = 1 << TWEN;
}

// Start Condition
void i2c_start()
{
	// Start
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while((TWCR & (1 << TWINT)) == 0);
}

// Stop Condition
void i2c_stop()
{
	// Stop
	TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
	while(TWCR & (1 << TWSTO));
}

// Write
void i2c_write(uint8_t d)
{
	// Setup for Transmission
	TWDR = d;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while((TWCR & (1 << TWINT)) == 0);
}

// Send Data Chunk
void i2c_send(uint8_t *d, uint8_t s)
{
	uint8_t i;
	for(i = 0; i < s; i = i + 1)			{ i2c_write(d[i]); }
}

// Read
uint8_t i2c_read(uint8_t ack)
{
	// Setup for Reception
	TWCR = (1 << TWINT) | (1 << TWEN) | ((1 & ack) << TWEA);
	while((TWCR & (1 << TWINT)) == 0);
	return TWDR;
}

// Generic Write Register
void i2c_write_reg(uint8_t addr, uint8_t reg, uint8_t val)
{
	// Start Transaction
	i2c_start();

	// Select Device
	i2c_write(addr);

	// Send Register & Value
	i2c_write(reg);
	i2c_write(val);

	// Terminate Transaction
	i2c_stop();
}

// Generic Read Register
uint8_t i2c_read_reg(uint8_t addr, uint8_t reg)
{
	uint8_t result;

	// Start Transaction
	i2c_start();

	// Select Device
	i2c_write(addr);

	// Select Register
	i2c_write(reg);

	// Terminate Transaction
	i2c_stop();

	// Re-Start Transaction
	i2c_start();

	// Select Device (READ)
	i2c_write(addr | 1);

	// Read
	result = i2c_read(0);

	// Terminate Transaction
	i2c_stop();

	return result;
}
