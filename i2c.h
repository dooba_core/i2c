/* Dooba SDK
 * I2C Communication
 */

#ifndef	__I2C_H
#define	__I2C_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Status Mask
#define	I2C_STATUS_MASK				0xf8

// Prescaler Value
#ifndef	I2C_PRESCALER
#define	I2C_PRESCALER				0x00
#endif

// Address Read Flag
#define	I2C_ADDR_READ				0x01

// Initialize I2C Interface
extern void i2c_init();

// Start Condition
extern void i2c_start();

// Stop Condition
extern void i2c_stop();

// Write
extern void i2c_write(uint8_t d);

// Send Data Chunk
extern void i2c_send(uint8_t *d, uint8_t s);

// Read
extern uint8_t i2c_read(uint8_t ack);

// Generic Write Register
extern void i2c_write_reg(uint8_t addr, uint8_t reg, uint8_t val);

// Generic Read Register
extern uint8_t i2c_read_reg(uint8_t addr, uint8_t reg);

#endif
